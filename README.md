# Plex Last.fm scrobbler

Last.fm scrobbler for Plex Media Server.

# Dependencies

wsta (https://github.com/esphen/wsta), xmllint, Hauzer's CLI Scrobbler (https://github.com/hauzer/scrobbler), 

# Usage

1. Install all needed dependencies.
2. Run and configure CLI Scrobbler
4. Save script somewhere and edit it to contain your last.fm username, Plex server address and your Plex token
5. Run it
6. Enjoy!