#!/bin/bash

user=''
server=''
token=''
xml='/tmp/scrobble.xml'

offset=0
wsta "ws://$server/:/websockets/notifications?X-Plex-Token=$token" | while read -r event; do
	if [[ $event != '?' ]]; then
		notif_type="$(echo "$event" | jq ".NotificationContainer.type")"
		if [[ $notif_type == '"playing"' ]]; then
			prev_key="$key"
			key="$(echo "$event" | jq ".NotificationContainer.PlaySessionStateNotification[0].key" | tr -d '"')"
			state="$(echo "$event" | jq ".NotificationContainer.PlaySessionStateNotification[0].state")"
			case $state in
				'"playing"')
					if [[ $key == $prev_key ]]; then
						prev_offset="$offset"
						offset="$(echo "$event" | jq ".NotificationContainer.PlaySessionStateNotification[0].viewOffset")"
						if (( $offset < $prev_offset )); then
							scrobbled=0
							epoch=0
						fi
						if [[ $scrobbled == 0 ]]; then
							if [[ $epoch != 0 ]]; then
								if (( $(( $(date +%s) - $epoch )) > $(( ${duration:0:-3} / 2 )) )); then
									duration_formated="$(printf '%dh:%dm:%ds\n' $((${duration:0:3}/3600)) $((${duration:0:3}%3600/60)) $((${duration:0:3}%60)))"
									scrobbler scrobble --album="$album" --duration="$duration_formated" -- "$user" "$artist" "$title" now
									scrobbled=1
								fi
							else
								epoch="$(date +%s)"
							fi
						fi
					else
						curl -s "http://$server$key?X-Plex-Token=$token" > "$xml"
						if [[ ! -z $(xmllint "$xml" --xpath '//MediaContainer/Track') ]]; then
							title="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@title)')"
							artist="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@grandparentTitle)')"
							album="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@parentTitle)')"
							duration="$(xmllint "$xml" --xpath 'string(//MediaContainer/Track/@duration)')"
							epoch="$(date +%s)"
							scrobbled=0
						fi
					fi
					scrobbler now-playing --album="$album" --duration="$duration" -- "$user" "$artist" "$title"
				;;
				'"paused"')
					[[ $key == $prev_key ]] && epoch=0
				;;
			esac
		fi
	fi
done